$(document).ready(function(){
	$('.js-promo-slider').slick({
		// autoplay: true,
		prevArrow: '<div class="slick-prev"><i class="icon icon_slider-arrow-prev"></i></div>',
		nextArrow: '<div class="slick-next"><i class="icon icon_slider-arrow-next"></i></div>'
	});
	$('.js-goods-slider').slick({
		// autoplay: true,
		slidesToShow: 4,
		prevArrow: '<div class="slick-prev"><i class="icon icon_slider-arrow-prev-small"></i></div>',
		nextArrow: '<div class="slick-next"><i class="icon icon_slider-arrow-next-small"></i></div>'
	});


	// Акордеон в сайдбаре
	$('.js-accordion-link > a').on('click', function(event){
		var $this = $(this).parent('.js-accordion-link');
		var $container = $this.find('.js-accordion-container').first();
		if ($container.length !== 0){
			event.preventDefault();
			if(!($this.hasClass('active'))){
				$this
					.addClass('active')
					.siblings('.js-accordion-link').removeClass('active')
					.find('.js-accordion-container').slideUp();
				$container.slideDown();
			}
			else {
				$this.removeClass('active');
				$container.slideUp();
			}
		}
	});


	// Раскрытие сайдбара 
	var catalog_data = $('body').data('sidebar');
	// console.log(catalog_data)
	if(catalog_data) {
		$('.js-accordion-link[data-sidebar="' + catalog_data + '"]').addClass('current');
	}

	$('.js-accordion-link.current').parents('.js-accordion-link').each(function(index, value){
		$(value).children('a').click();
	});

	// Квадратики с цветом
	// var $('.ui-color-square')
	$('.ui-color-square').each(function(index, value){
		$(value).css('background-color', $(value).data('color'));
		// console.log(value);
	});

	$('.js-filter').css('height', ($('.catalog-filter-container').height() + 70));
	$('.js-filter-open').on('click', function(){
		$('.js-filter').removeClass('closed').css('height', ($('.catalog-filter-container').height() + 70));
	});
	$('.js-filter-close').on('click', function(){
		$('.js-filter').addClass('closed').css('height', $('.js-filter-open').height());
	});

	// табы
	$('.js-tabs').on('click', '.js-tabs-header', function(){
		var $this = $(this);
		var tab_container = $this.parents('.js-tabs');
		var active_header = tab_container.find('.js-tabs-header.active');
		var active_content = tab_container.find('.js-tabs-content.active');
		var tab_id = $this.index();
		if(!($this.hasClass('active')) && $('.js-tabs-content').eq(tab_id).length != 0){
			active_header.removeClass('active');
			active_content.removeClass('active');
			tab_container.find('.js-tabs-content:nth-child(' + (tab_id + 1) + ')').addClass('active');
			$this.addClass('active');
		}
	});

	// открытие формы по клику на цвет из палитры
	$('.js-color').on('click', '.js-color-btn', function(){
		$('.js-color').removeClass('opened');
		$('#overlay').remove();
		$('body').removeClass('overlayed');
		
		$(this).parent('.js-color').addClass('opened');
		$('body').prepend('<div id="overlay"></div>');
		$('body').addClass('overlayed');
	});
	$('body').on('click', '#overlay', function(){
		// console.log('123');
		$('.js-color').removeClass('opened');
		$('#overlay').remove();
		$('body').removeClass('overlayed');
	});
	$('.js-color-close').on('click', function(){
		$('.js-color').removeClass('opened');
		$('#overlay').remove();
		$('body').removeClass('overlayed');
	});

	// Слайдер в карточке товара ниток
	$('.js-palette-slider').slick({
		rows: 3,
		slidesPerRow: 10,
		fade: true,
		prevArrow: "<div class='slick-prev slick-arrow'><i class='icon icon_arrow-prev'></i></div>",
		nextArrow: "<div class='slick-next slick-arrow'><i class='icon icon_arrow-next'></i></div>",
		appendArrows: $('.js-palette-slider')
	});


	// Слайдер в карточке товара машин
	$('.js-product_image-slider').slick({
		arrows: false,
		asNavFor: '.js-product_image-slider-preview',
		fade: true
	});
	$('.js-product_image-slider-preview').slick({
		prevArrow: "<div class='slick-prev slick-arrow'><i class='icon icon_arrow-prev'></i></div>",
		nextArrow: "<div class='slick-next slick-arrow'><i class='icon icon_arrow-next'></i></div>",
		asNavFor: '.js-product_image-slider',
		focusOnSelect: true,
		slidesToShow: 3,
		centerMode: true,
		centerPadding: 0
	});
});
